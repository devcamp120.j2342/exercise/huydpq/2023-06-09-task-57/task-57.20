package com.devcamp.task57d20.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task57d20.Service.DrinkService;
import com.devcamp.task57d20.models.Drink;

@RestController
@CrossOrigin
public class DrinkController {
    @Autowired
    DrinkService drinkService;
    @GetMapping("/drinks")
    public ArrayList<Drink> getDrink() {
        

        return drinkService.getAllDrink();
    }
}
