package com.devcamp.task57d20.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task57d20.Service.CMenuService;
import com.devcamp.task57d20.models.CMenu;

@RestController
@CrossOrigin
@RequestMapping("/")
public class MenuController {
    @Autowired
    CMenuService cMenuService;

    
    @GetMapping("/combomenu")
    public ArrayList<CMenu> getAllMenus() {
        return  cMenuService.getAllCMenusService();
    }    
}


