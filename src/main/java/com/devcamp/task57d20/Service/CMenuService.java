package com.devcamp.task57d20.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.task57d20.models.CMenu;

@Service
public class CMenuService {
    CMenu sizeS = new CMenu('S', 20, 2, 200, 2, 150000);
    CMenu sizeM = new CMenu('M', 25, 4, 300, 3, 200000);
    CMenu sizeL = new CMenu('L', 30, 8, 500, 4, 250000);

    public ArrayList<CMenu> getAllCMenusService() {
        ArrayList<CMenu> allMenus = new ArrayList<>();

        allMenus.add(sizeS);
        allMenus.add(sizeM);
        allMenus.add(sizeL);

        return allMenus;
    }
}
