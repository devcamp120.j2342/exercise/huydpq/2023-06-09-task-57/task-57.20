package com.devcamp.task57d20.Service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.stereotype.Service;
import com.devcamp.task57d20.models.Drink;

@Service
public class DrinkService {
    Drink drink1 = new Drink("TRATAC", "Trà tắc", 10000, null, new Date(1615177934000L), new Date(1615177934000L));
    Drink drink2 = new Drink("COCA", "Cocacola", 15000, null, new Date(1615177934000L), new Date(1615177934000L));
    Drink drink3 = new Drink("PEPSI", "Pepsi", 15000, null, new Date(1615177934000L), new Date(1615177934000L));
    Drink drink4 = new Drink("LAVIE", "Lavie", 5000, null, new Date(1615177934000L), new Date(1615177934000L));
    Drink drink5 = new Drink("TRASUA", "Trà sữa trân châu", 40000, null, new Date(1615177934000L),
            new Date(1615177934000L));
    Drink drink6 = new Drink("FANTA", "Fanta", 15000, null, new Date(1615177934000L), new Date(1615177934000L));

    public ArrayList<Drink> getAllDrink() {
        ArrayList<Drink> allDrink = new ArrayList<>();
        allDrink.add(drink1);
        allDrink.add(drink2);
        allDrink.add(drink3);
        allDrink.add(drink4);
        allDrink.add(drink5);
        allDrink.add(drink6);

        return allDrink;
    }
}
