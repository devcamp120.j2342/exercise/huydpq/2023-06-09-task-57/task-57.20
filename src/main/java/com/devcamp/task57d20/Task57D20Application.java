package com.devcamp.task57d20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task57D20Application {

	public static void main(String[] args) {
		SpringApplication.run(Task57D20Application.class, args);
	}

}
